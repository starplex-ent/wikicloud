package com.starplex.wikicloud;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.BaseAdapter;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.FileInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public abstract class CommonAdapter extends BaseAdapter {

    public enum ObjectType {NONE, GRID_LINK, WEB_LINK, ARTICLE, FAVOR, ADS, EXTERNAL_APP, SETTINGS}

    @Nullable
    protected Context context;
    public String title;
    public Bitmap imgBMPs[];
    public String imgBMPPaths[];
    public String imgTexts[]; // labels under image
    public ObjectType imgTypes[];
    public String imgStrs[]; // URL of link
    public boolean enableBackground;

    public CommonAdapter(@Nullable Context context, String indexFileName, boolean loadImages) {
        this(context, indexFileName, loadImages, null);
    }

    public CommonAdapter(@Nullable Context context, String indexFileName, boolean loadImages, String languageCode) {
        this.context = context;

        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        try {
            SAXParser saxParser = spf.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();
            SAXListsParser saxListsParser = new SAXListsParser(this);
            saxListsParser.setUiModeEnabled(loadImages);
            saxListsParser.setCurstomLanguageCode(languageCode);
            xmlReader.setContentHandler(saxListsParser);
            xmlReader.parse(new InputSource(new FileInputStream(indexFileName)));
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.getLocalizedMessage(), e);
        }
    }

    @Override
    final public int getCount() {
        if (imgBMPs != null)
            return imgBMPs.length;
        else
            return 0;
    }
}


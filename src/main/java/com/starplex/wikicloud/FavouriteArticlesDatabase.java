package com.starplex.wikicloud;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FavouriteArticlesDatabase extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "favourite.db";
	private static final int DATABASE_VERSION = 2;
	
	public static final String TABLE_NAME = "favourites";
	public static final String ARTICLE_TITLE = "title";
	public static final String ARTICLE_URL = "url";
	public static final String UID = "_id";
	
	private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " +
					TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ ARTICLE_TITLE + " VARCHAR(255)," + ARTICLE_URL + " VARCHAR(255));";
	
	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;
	
	public FavouriteArticlesDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		db.execSQL(SQL_DELETE_ENTRIES);
		onCreate(db);
	}
}

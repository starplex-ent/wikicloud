package com.starplex.wikicloud;

import android.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.starplex.wikicloud.CommonAdapter;
import com.starplex.wikicloud.MenuActivity;

final class GridItemListener implements OnItemClickListener {
    private CommonAdapter gridAdapter;
    private MenuActivity activity;
    private boolean isInDrawer;

    public GridItemListener(MenuActivity activity, CommonAdapter gridAdapter, boolean isInDrawer) {
        this.gridAdapter = gridAdapter;
        this.activity = activity;
        this.isInDrawer = isInDrawer;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position,
                            long id) {
        if (isInDrawer) {
            if (!activity.useTabletLayout) {
                //closing drawer
                if (activity.findViewById(R.id.drawer) != null) {
                    //noinspection ConstantConditions
                    ((DrawerLayout) activity.findViewById(R.id.drawer)).closeDrawers();
                }
            }
            // clearing fragment stack
            FragmentManager fm = activity.getFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
                fm.popBackStack();
            }
        }
        switch (gridAdapter.imgTypes[position]) {
            case WEB_LINK:
                activity.openBrowser(gridAdapter.imgStrs[position]);

                break;
            case GRID_LINK:
                activity.openGrid(gridAdapter.imgStrs[position]);

                break;
            case ARTICLE:
                activity.openArticle(gridAdapter.imgStrs[position]);

                break;
            case FAVOR:
                activity.openFavourites();

                break;
            case ADS:
                activity.removeAds();
                break;
            case EXTERNAL_APP:
                activity.openExternalAppOrGooglePlay(gridAdapter.imgStrs[position]);
                break;
            case SETTINGS:
                activity.openSettings();
                break;
            default:
                break;
        }
    }
}
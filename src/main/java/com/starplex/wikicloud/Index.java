package com.starplex.wikicloud;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Class encapsulating index of wiki's articles.
 * <p>
 * Note that since it is marked as {@link Serializable} it's members shouldn't be changed to
 * support backward compatibility
 */
public class Index implements Serializable {

    /**
     * List of article titles
     */
    public List<String> articleTitles = new LinkedList<>();

    /**
     * List of article paths
     */
    public List<String> articlePaths = new LinkedList<>();

    /**
     * Article image paths
     */
    public List<String> articleImagePaths = new LinkedList<>();
}

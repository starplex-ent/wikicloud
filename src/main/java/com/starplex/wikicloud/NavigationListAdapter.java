package com.starplex.wikicloud;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.starplex.wikicloud.CommonAdapter;

public class NavigationListAdapter extends CommonAdapter {

	public NavigationListAdapter(Context c, String indexFileName) {
		super(c, indexFileName, false);
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("PrivateResource")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textView;
		
		if (convertView == null) {  // if it's not recycled, initialize some attributes
            textView = new TextView(context);
        } else {
            textView = (TextView) convertView;
        }
		
		textView.setText(imgTexts[position]);
		textView.setGravity(Gravity.START);
		textView.setPadding(dpToPx(7.5f), dpToPx(16), dpToPx(15), dpToPx(15));
		BitmapDrawable drawable = null;
		if (context != null) {
            if (imgBMPs[position] == null) {
                imgBMPs[position] = BitmapFactory.decodeFile(imgBMPPaths[position]);
            }
            drawable = new BitmapDrawable(context.getResources(), imgBMPs[position]);
			drawable.setBounds(0, 0, dpToPx(24), dpToPx(24));
		}
		textView.setCompoundDrawablePadding(dpToPx(5));
		textView.setCompoundDrawables(drawable, null, null, null);
		if (Build.VERSION.SDK_INT < 23) {
			//noinspection deprecation
			textView.setTextAppearance(context, android.R.style.TextAppearance_DeviceDefault_Medium_Inverse);
		} else {
			textView.setTextAppearance(android.R.style.TextAppearance_DeviceDefault_Medium_Inverse);
		}
        textView.setTextColor(context.getResources().getColor(R.color.primary_text_default_material_dark));
		
		return textView;
	}
	
	@SuppressWarnings("ConstantConditions")
    int dpToPx(float dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int)(dp*scale+0.5f);
	}

}

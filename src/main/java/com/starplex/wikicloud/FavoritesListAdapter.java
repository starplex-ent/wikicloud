package com.starplex.wikicloud;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.starplex.wikicloud.FavouriteArticle;

public class FavoritesListAdapter extends BaseAdapter {

	List<FavouriteArticle> favouriteArticles;
	Context mContext;
	
	public FavoritesListAdapter(Context context, List<FavouriteArticle> favouriteArticles2) {
		this.favouriteArticles = favouriteArticles2;
		mContext = context;
	}

	@Override
	public int getCount() {
		return favouriteArticles.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textView;
		
		if (convertView == null) {  // if it's not recycled, initialize some attributes
            textView = new TextView(mContext);
        } else {
            textView = (TextView) convertView;
        }
		
		textView.setText(favouriteArticles.get(position).title);
		textView.setPadding(10, 7, 7, 7);
		textView.setTextColor(Color.BLACK);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
		
		return textView;
	}
}

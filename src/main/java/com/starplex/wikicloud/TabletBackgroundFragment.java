package com.starplex.wikicloud;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TabletBackgroundFragment extends Fragment {
    @Override
    public void onAttach(Context context) {
        if (!(context instanceof AbstractWikiActivity)) {
            throw new RuntimeException("TabletBackgroundFragment must be attached only to AbstractWikiActivity descendants");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_tablet_background, container, false);
        AbstractWikiActivity activity = (AbstractWikiActivity) getActivity();
        TextView backgroundFragmentTextView = (TextView) layout.findViewById(R.id.background_fragment_text);
        if (backgroundFragmentTextView != null) {
            backgroundFragmentTextView.setText(activity.getString(AbstractWikiActivity.STRING_SELECT_SECTION));
        }

        activity.setTitle(activity.getString(AbstractWikiActivity.STRING_APP_TITLE));

        return layout;
    }
}

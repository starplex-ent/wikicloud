package com.starplex.wikicloud;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class FavoritesFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							Bundle savedInstanceBundle) {
		View view = inflater.inflate(R.layout.favourites_fragment, container, false);
		
		ListView lv = (ListView)view.findViewById(R.id.listView1);
		synchronized (((MenuActivity)getActivity()).dbMutex) {
			lv.setAdapter(new FavoritesListAdapter(getActivity(), ((MenuActivity) getActivity()).favouriteArticles));
		}
		lv.setOnItemClickListener(new FavouritesListItemListener());
		
		getActivity().setTitle(((AbstractWikiActivity)getActivity()).getString(AbstractWikiActivity.STRING_FAVORITES));
		
		return view;
	}

	class FavouritesListItemListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			MenuActivity mActivity = (MenuActivity)FavoritesFragment.this.getActivity();
			synchronized (mActivity.dbMutex) {
				mActivity.openArticle(mActivity.favouriteArticles.get(position).article_path);
			}
			
		}
		
	}
}

package com.starplex.wikicloud;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class MenuActivity extends AbstractWikiActivity implements DownloadingRetainedFragment.OnDownloadingFragmentInteractionListener {

    private boolean isDestroyed = true;

    private boolean isWikiLoaded = false;
    private RequestQueue defaultQueue;
    private static DownloadingRetainedFragment downloadingFragment = null;
    private ProgressDialog downloadingDialog = null;
    private boolean isAlphaModeEnabled = false;

    ServiceConnection mServiceConn;

    final Object dbMutex = new Object();
    List<FavouriteArticle> favouriteArticles = new LinkedList<>();
    FavouriteArticlesDatabase dbHelper;

    //protected AdRequest adRequest;
    protected IInAppBillingService mService;
    AdView mAdView;
    boolean removeadsBought = false;
    protected DrawerLayout mDrawerLayout;

    protected ActionBarDrawerToggle mDrawerToggle;
    boolean useTabletLayout;

    @Nullable
    Index searchIndex;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isDestroyed = false;

        defaultQueue = Volley.newRequestQueue(this);

        String[] filesArray = fileList();
        List<String> filesList = Arrays.asList(filesArray);
//        boolean b = (filesList.contains("ru") || filesList.contains("en") || filesList.contains("fr"));
        boolean b = false;
        for (String lang :
                getWikiTranslationLanguagesList()) {
            b |= filesList.contains(lang);
        }
        if (!b) {
            setContentView(R.layout.activity_download);
            //noinspection ConstantConditions
            ((TextView) findViewById(R.id.textView)).setText(getString(STRING_SELECT_LANGUAGE_WIKI));
            TextView noConnectionTextView = (TextView) findViewById(R.id.no_connection_textview);
            //noinspection ConstantConditions
            noConnectionTextView.setText(getString(STRING_NO_CONNECTION));
            Button retryButton = (Button) findViewById(R.id.retry_button);
            //noinspection ConstantConditions
            retryButton.setText(getString(STRING_RETRY_BUTTON));

            if (downloadingDialog == null || savedInstanceState == null) {
                startLangListLoadingTask(null);
            } else {
                openDownloadingDialog();
            }
        } else {
            isWikiLoaded = true;

            setContentView(R.layout.activity_menu);

            useTabletLayout = findViewById(R.id.drawer) == null;

            if (savedInstanceState == null) {
                if (!useTabletLayout) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    GridFragment fragment = new GridFragment();
                    Bundle extra = new Bundle();
                    extra.putString("url", Utils.getWikiFolderNamePrefix(this) + "main/index.xml");
                    fragment.setArguments(extra);
                    fragmentTransaction.add(R.id.vgl, fragment);
                    fragmentTransaction.commit();
                } else {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    TabletBackgroundFragment fragment = new TabletBackgroundFragment();
                    fragmentTransaction.add(R.id.vgl, fragment);
                    fragmentTransaction.commit();
                }
            }

            CommonAdapter drawerAdapter;
            if (useTabletLayout) {
                GridView drawer = (GridView) findViewById(R.id.left_drawer);
                drawerAdapter = new GridAdapter(this, Utils.getWikiFolderNamePrefix(this) + "main/index.xml");
                if (drawer != null) {
                    drawer.setAdapter(drawerAdapter);
                    drawer.setOnItemClickListener(new GridItemListener(this, drawerAdapter, true));
                }
            } else {
                ListView drawer = (ListView) findViewById(R.id.left_drawer);
                drawerAdapter = new NavigationListAdapter(this, Utils.getWikiFolderNamePrefix(this) + "main/drawer.xml");
                if (drawer != null) {
                    drawer.setAdapter(drawerAdapter);
                    drawer.setOnItemClickListener(new GridItemListener(this, drawerAdapter, true));
                }
            }

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    loadDatabase();

                    return null;
                }
            }.execute();

            if (!useTabletLayout) {
                // set up drawer layout action bar button
                mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
                mDrawerToggle = new ActionBarDrawerToggle(
                        this,                  /* host Activity */
                        mDrawerLayout,         /* DrawerLayout object */
                        getStringId(STRINGID_DRAWER_OPEN),  /* "open drawer" description */
                        getStringId(STRINGID_DRAWER_CLOSE)  /* "close drawer" description */
                ) {

                    /**
                     * Called when a drawer has settled in a completely closed state.
                     */
                    public void onDrawerClosed(View view) {
                        super.onDrawerClosed(view);
                    }

                    /**
                     * Called when a drawer has settled in a completely open state.
                     */
                    public void onDrawerOpened(View drawerView) {
                        super.onDrawerOpened(drawerView);
                    }
                };
                //noinspection ConstantConditions
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                //noinspection deprecation
                mDrawerLayout.setDrawerListener(mDrawerToggle);
            }

            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                if (!useTabletLayout)
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
            }

            mAdView = (AdView) findViewById(R.id.adView);
            if (!getAdUnitId().equals(mAdView.getAdUnitId())) {
                throw new MismatchingAdUnitIdException();
            }
            mServiceConn = new ServiceConnection() {
                @Override
                public void onServiceDisconnected(ComponentName name) {
                    mService = null;
                }

                @Override
                public void onServiceConnected(ComponentName arg0, IBinder service) {
                    mService = IInAppBillingService.Stub.asInterface(service);

                    try {
                        Bundle ownedItems = mService.getPurchases(3, getWikiPackageName(), "inapp", null);

                        int response = ownedItems.getInt("RESPONSE_CODE");
                        if (response == 0) {
                            ArrayList<String> ownedSkus =
                                    ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                            if (ownedSkus != null && ownedSkus.size() == 1) {
                                removeadsBought = true;
                                if (mAdView != null)
                                    try {
                                        ((ViewManager) mAdView.getParent()).removeView(mAdView);
                                    } catch (NullPointerException ignored) {
                                    }
                            }
                        }
                    } catch (RemoteException | NullPointerException e) {
                        Log.e("Ads", e.getMessage() == null ? "Exception" : e.getMessage());
                    }
                }
            };

            Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            serviceIntent.setPackage("com.android.vending");
            bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

            if (!Utils.hasNetworkConnection(this)) {
                final AdView ad = (com.google.android.gms.ads.AdView) findViewById(R.id.adView);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.root_layout);
                if (layout != null) {
                    layout.removeView(ad);
                }
            } else {
                // check for updates
                if (savedInstanceState == null) {
                    checkForUpdates();
                }
            }

            if (savedInstanceState == null) {
                SharedPreferences preferences = getSharedPreferences("MenuActivity", MODE_PRIVATE);
                int curCounter = preferences.getInt("curCounter", 0);
                if (curCounter == 5) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(getString(STRING_RATE_DIALOGUE_TITLE))
                            .setMessage(getString(STRING_RATE_DIALOGUE_TEXT))
                            .setIcon(getIconId())
                            .setCancelable(true)
                            .setPositiveButton(getString(STRING_OK), new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(getAppURLOnGooglePlay()));
                                    startActivity(i);

                                    SharedPreferences preferences = getSharedPreferences("MenuActivity", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putInt("curCounter", 6);
                                    editor.apply();
                                }
                            })
                            .setNegativeButton(getString(STRING_DONT_ASK), new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences preferences = getSharedPreferences("MenuActivity", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putInt("curCounter", 6);
                                    editor.apply();
                                }
                            })
                            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    SharedPreferences preferences = getSharedPreferences("MenuActivity", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putInt("curCounter", 5);
                                    editor.apply();
                                }
                            });
                    builder.create().show();
                } else if (curCounter != 6) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("curCounter", curCounter + 1);
                    editor.apply();
                }
            }

            // load search index
            if (savedInstanceState == null) {
                new AsyncTask<Void, Void, Index>() {

                    @Override
                    protected Index doInBackground(Void... params) {
                        Index index = null;
                        try {
                            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(Utils.getWikiFolderNamePrefix(MenuActivity.this) + "search.index"));
                            index = (Index) ois.readObject();
                            ois.close();
                        } catch (IOException | ClassNotFoundException ignored) {
                        }

                        return index;
                    }

                    @Override
                    protected void onPostExecute(Index index) {
                        searchIndex = index;
                    }
                }.execute();
            } else {
                searchIndex = (Index) savedInstanceState.getSerializable("search_index");
            }

            MobileAds.initialize(getApplicationContext(), getAdApplicationId());
        }
    }

    private void loadDatabase() {
        synchronized (dbMutex) {
            dbHelper = new FavouriteArticlesDatabase(this);

            String[] projection = {
                    FavouriteArticlesDatabase.UID,
                    FavouriteArticlesDatabase.ARTICLE_TITLE,
                    FavouriteArticlesDatabase.ARTICLE_URL
            };

            SQLiteDatabase db = dbHelper.getReadableDatabase();

            Cursor cursor = db.query(
                    FavouriteArticlesDatabase.TABLE_NAME,  // The table to query
                    projection,                               // The columns to return
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            );

            while (cursor.moveToNext()) {
                FavouriteArticle article = new FavouriteArticle();
                article.title = cursor.getString(cursor.getColumnIndex(FavouriteArticlesDatabase.ARTICLE_TITLE));
                article.article_path = cursor.getString(cursor.getColumnIndex(FavouriteArticlesDatabase.ARTICLE_URL));
                article.id = cursor.getLong(cursor.getColumnIndex(FavouriteArticlesDatabase.UID));
                favouriteArticles.add(article);
            }
            cursor.close();
        }
    }

    private void checkForUpdates() {
        defaultQueue.add(new JsonObjectRequest(isAlphaModeEnabled ? getAlphaUpdateSourceURL() : getStableUpdateSourceURL(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                SharedPreferences preferences = getSharedPreferences("MenuActivity", MODE_PRIVATE);
                int currentVersion = preferences.getInt("wiki_version", 1);
                final String langCode = preferences.getString("wiki_lang", "en");

                try {
                    final int newVersion = response.getInt("version");
                    if (newVersion > currentVersion) {
                        JSONArray languagesJsonArray = response.getJSONArray("languages");
                        JSONObject langJsonObject = null;
                        for (int i = 0; i < languagesJsonArray.length(); i++) {
                            JSONObject langObject = languagesJsonArray.getJSONObject(i);
                            if (langObject.getString("code").equals(langCode)) {
                                langJsonObject = langObject;
                                break;
                            }
                        }

                        if (langJsonObject != null) {
                            final String link = langJsonObject.getString("link");

                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MenuActivity.this);
                            alertBuilder.setTitle(getString(STRING_NEW_UPDATE));
                            alertBuilder.setMessage(getString(STRING_UPDATE_DESCRIPTION) +
                                    (langJsonObject.getString("update_description") == null ? "" : langJsonObject.getString("update_description")));
                            alertBuilder.setNegativeButton(getString(STRING_UPDATE_LATER), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertBuilder.setPositiveButton(getString(STRING_UPDATE), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //noinspection ResultOfMethodCallIgnored
                                    try {
                                        FileUtils.deleteDirectory(new File(getFilesDir().toString() + "/" + langCode));
                                    } catch (IOException ignored) {
                                    }
                                    startLanguageLoading(langCode, link, newVersion);
                                }
                            });
                            alertBuilder.show();
                        }
                    }
                } catch (JSONException ignored) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }));
    }

    public void startLangListLoadingTask(View unused) {
        View noConnectionTextView = findViewById(R.id.no_connection_textview);
        if (noConnectionTextView != null) {
            noConnectionTextView.setVisibility(View.GONE);
        }
        View langListView = findViewById(R.id.langListView);
        if (langListView != null) {
            langListView.setVisibility(View.VISIBLE);
        }
        Button retryButton = (Button) findViewById(R.id.retry_button);
        if (retryButton != null) {
            retryButton.setVisibility(View.GONE);
        }

        final ProgressDialog progressDialog = new ProgressDialog(MenuActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(STRING_LOADING));
        progressDialog.setCancelable(false);
        progressDialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, isAlphaModeEnabled ? getAlphaUpdateSourceURL() : getStableUpdateSourceURL(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                if (!MenuActivity.this.isDestroyed) {
                    progressDialog.dismiss();

                    View noConnectionTextView = findViewById(R.id.no_connection_textview);
                    if (noConnectionTextView != null) {
                        noConnectionTextView.setVisibility(View.GONE);
                    }
                    ListView langListView = (ListView) findViewById(R.id.langListView);
                    if (langListView != null) {
                        langListView.setVisibility(View.VISIBLE);

                        try {
                            final JSONArray langArrayJson = response.getJSONArray("languages");
                            String[] languages = new String[langArrayJson.length()];
                            for (int i = 0; i < languages.length; i++) {
                                languages[i] = langArrayJson.getJSONObject(i).getString("description");
                            }
                            langListView.setAdapter(new ArrayAdapter<>(MenuActivity.this, R.layout.lang_list_item, languages));
                            langListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    try {
                                        JSONObject jsonObject = langArrayJson.getJSONObject(position);
                                        String link = jsonObject.getString("link");
                                        String languageCode = jsonObject.getString("code");

                                        startLanguageLoading(languageCode, link, response.getInt("version"));
                                    } catch (JSONException e) {
                                        new AlertDialog.Builder(MenuActivity.this)
                                                .setMessage(getString(STRING_CANNOT_DOWNLOAD_LANG))
                                                .setPositiveButton(getString(STRING_OK), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                })
                                                .create().show();
                                    }
                                }
                            });
                        } catch (JSONException e) {
                            failedToLoadLangList();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                if (!MenuActivity.this.isDestroyed) {
                    failedToLoadLangList();
                }
            }
        });
        defaultQueue.add(request);
    }

    public void startLanguageLoading(String code, String link, int version) {
        openDownloadingDialog();

        downloadingFragment = DownloadingRetainedFragment.newInstance(code, link, getFilesDir().getAbsolutePath() + "/", version);
        getSupportFragmentManager().beginTransaction().add(downloadingFragment,  DownloadingRetainedFragment.TAG).commit();
    }

    private void openDownloadingDialog() {
        downloadingDialog = new ProgressDialog(this);
        downloadingDialog.setMessage(getString(STRING_LOADING));
        downloadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadingDialog.setProgress(0);
        downloadingDialog.setMax(110);
        downloadingDialog.setCancelable(false);
        downloadingDialog.show();
    }

    private void failedToLoadLangList() {
        View noConnectionTextView = findViewById(R.id.no_connection_textview);
        if (noConnectionTextView != null) {
            noConnectionTextView.setVisibility(View.VISIBLE);
        }
        View langListView = findViewById(R.id.langListView);
        if (langListView != null) {
            langListView.setVisibility(View.GONE);

        }
        Button retryButton = (Button) findViewById(R.id.retry_button);
        if (retryButton != null) {
            retryButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (isWikiLoaded) {
            if (!useTabletLayout) {
                mDrawerToggle.syncState();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (isWikiLoaded) {
            if (Utils.hasNetworkConnection(this)) {
                mAdView.loadAd(new AdRequest.Builder().build());
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (isWikiLoaded) {
            if (!useTabletLayout) {
                mDrawerToggle.onConfigurationChanged(newConfig);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (isWikiLoaded) {
            int id = item.getItemId();
            switch (id) {
                case android.R.id.home:
                    if (getFragmentManager().getBackStackEntryCount() == 0) {
                        if (!useTabletLayout) {
                            if (mDrawerToggle.onOptionsItemSelected(item)) {
                                return true;
                            }
                        }
                    } else {
                        getFragmentManager().popBackStack();
                        if (useTabletLayout) {
                            if (getFragmentManager().getBackStackEntryCount() <= 2) {
                                if (getSupportActionBar() != null) {
                                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                                }
                            }
                        } else {
                            if (getFragmentManager().getBackStackEntryCount() == 1) {
                                mDrawerToggle.setDrawerIndicatorEnabled(true);
                            }
                        }
                    }

                    return true;
                default:
                    break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("search_index", searchIndex);
    }

    @Override
    public void onBackPressed() {
        if (isWikiLoaded) {
            if (!useTabletLayout) {
                DrawerLayout layout = (DrawerLayout) findViewById(R.id.drawer);
                View leftDrawer = findViewById(R.id.left_drawer);
                //noinspection ConstantConditions
                if (leftDrawer != null && layout.isDrawerOpen(leftDrawer)) {
                    layout.closeDrawer(leftDrawer);
                } else {
                    if (getFragmentManager().getBackStackEntryCount() >= 1) {
                        if (getFragmentManager().getBackStackEntryCount() == 1) {
                            mDrawerToggle.setDrawerIndicatorEnabled(true);
                        }
                        getFragmentManager().popBackStack();
                    } else
                        super.onBackPressed();
                }
            } else {
                if (getFragmentManager().getBackStackEntryCount() >= 2) {
                    if (getSupportActionBar() != null && getFragmentManager().getBackStackEntryCount() == 2) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    }
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);

        if (Intent.ACTION_VIEW.toLowerCase().equals(getIntent().getAction().toLowerCase())) {
            String data = getIntent().getDataString();
            openArticle(data);
        } else if (intent.getAction().toLowerCase().equals(Intent.ACTION_SEARCH.toLowerCase())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Index resultSet = SearchIndexer.search(searchIndex, query);
            if (resultSet.articleTitles.size() == 1) {
                openArticle(resultSet.articlePaths.get(0));
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Utils.cleanCachedPrefix();

        isDestroyed = true;

        if (isWikiLoaded) {
            dbHelper.close();
            if (mService != null) {
                unbindService(mServiceConn);
            }
        }
    }

    public void openBrowser(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        try {
            startActivity(browserIntent);
        } catch (ActivityNotFoundException e) {
            Log.e(MenuActivity.class.getSimpleName(), e.getMessage());
        }
    }

    public void openGrid(String url) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        GridFragment fragment = new GridFragment();
        Bundle extra = new Bundle();
        extra.putString("url", Utils.getWikiFolderNamePrefix(this) + url);
        fragment.setArguments(extra);
        fragmentTransaction.replace(R.id.vgl, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        if (!useTabletLayout) {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
        }
    }

    public void openArticle(String url) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        WebFragment fragment = new WebFragment();
        Bundle extra = new Bundle();
        if (url.startsWith("file:///android_asset/")) {
            //noinspection ConstantConditions
            url = url.replace("file:///android_asset/", "file://" + Utils.getWikiFolderNamePrefix(this));
        } else {
            url = "file://" + Utils.getWikiFolderNamePrefix(this) + url;
        }
        extra.putString("url", url);
        fragment.setArguments(extra);
        fragmentTransaction.replace(R.id.vgl, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!useTabletLayout) {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
        }
    }

    public void openFavourites() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FavoritesFragment fragment = new FavoritesFragment();
        fragmentTransaction.replace(R.id.vgl, fragment, null);
        if (!useTabletLayout)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!useTabletLayout) {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
        }
    }

    public void openExternalAppOrGooglePlay(String packageName) {
        if (Utils.isPackageInstalled(packageName, this)) {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(packageName);
            startActivity(launchIntent);
        } else {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName));
            startActivity(browserIntent);
        }
    }

    public void openSettings() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SettingsFragment fragment = new SettingsFragment();
        fragmentTransaction.replace(R.id.vgl, fragment, null);
        if (!useTabletLayout)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (!useTabletLayout) {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
        }
    }

    public void removeAds() {
        try {
            if (mService != null) {
                if (removeadsBought) {
                    Toast.makeText(this, getString(STRING_ALREADY_PURCHASED), Toast.LENGTH_LONG).show();
                } else {
                    Bundle buyIntentBundle = mService.getBuyIntent(3, getWikiPackageName(),
                            "removeads", "inapp", null);
                    PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                    //noinspection ConstantConditions
                    startIntentSenderForResult(pendingIntent.getIntentSender(),
                            1001, new Intent(), 0, 0,
                            0);
                }
            }
        } catch (Exception e) {
            //Log.e("Ads", e.getLocalizedMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            data.getStringExtra("INAPP_PURCHASE_DATA");
            data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (responseCode == RESULT_OK) {
                try {
                    if (mAdView != null) {
                        ((ViewManager) mAdView.getParent()).removeView(mAdView);
                    }
                } catch (Exception e) {
                    Log.e(this.getClass().getName(), e.getMessage());
                }
                Toast.makeText(this, getString(STRING_RESTART_APP), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onDownloadingProgress(int progress) {
        if (downloadingDialog != null) {
            downloadingDialog.setProgress(progress);
        } else {
            openDownloadingDialog();
            downloadingDialog.setProgress(progress);
        }
    }

    @Override
    public void onDownloadingCompleted(String langCode, int version) {
        Utils.cleanCachedPrefix();

        getSupportFragmentManager().beginTransaction().remove(downloadingFragment).commitAllowingStateLoss();

        SharedPreferences.Editor editor = getSharedPreferences("MenuActivity", MODE_PRIVATE).edit();
        editor.putInt("wiki_version", version);
        editor.putString("wiki_lang", langCode);
        editor.apply();

        FirebaseAnalytics.getInstance(this).logEvent("wiki_downloaded", new Bundle());

        finish();
        startActivity(getIntent());
    }

    @Override
    public void onDownloadingFailed() {
        Utils.cleanCachedPrefix();

        if (downloadingDialog != null) {
            downloadingDialog.dismiss();
            downloadingDialog = null;
            getSupportFragmentManager().beginTransaction().remove(downloadingFragment).commitAllowingStateLoss();

            new AlertDialog.Builder(this)
                    .setMessage(getString(STRING_DOWNLOADING_WIKI_FAILED))
                    .setPositiveButton(getString(STRING_OK), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .show();
            startLangListLoadingTask(null);
        }
    }

    private int wikiReinstallationCancellationCounter = 0;

    public void reinstallWiki(View view) {
        new AlertDialog.Builder(this)
                .setTitle(getString(STRING_REINSTALL_WIKI_QUESTION_TITLE))
                .setMessage(getString(STRING_REINSTALL_WIKI_QUESTION_MESSAGE))
                .setPositiveButton(getString(STRING_REINSTALL), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            FileUtils.deleteDirectory(new File(Utils.getWikiFolderNamePrefix(MenuActivity.this)));
                        } catch (IOException e) {
                            Log.i("CoCWiki", Log.getStackTraceString(e));
                        } finally {
                            getSharedPreferences("MenuActivity", MODE_PRIVATE).edit().putString("wiki_lang", null).putInt("wiki_version", 0).apply();
                            Utils.cleanCachedPrefix();
                            finish();
                            startActivity(getIntent());
                        }
                    }
                })
                .setNegativeButton(getString(STRING_CANCEL), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        wikiReinstallationCancellationCounter++;
                        if (wikiReinstallationCancellationCounter >= 10) {
                            isAlphaModeEnabled = true;
                            Toast.makeText(MenuActivity.this, "Alpha Mode Enabled", Toast.LENGTH_SHORT).show();
                            checkForUpdates();
                            wikiReinstallationCancellationCounter = 0;
                        }
                    }
                })
                .show();

    }

}

package com.starplex.wikicloud;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Utils {
    public static boolean hasNetworkConnection(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null)
            if (netInfo.isConnectedOrConnecting()) {
                return true;
            }
        return false;
    }

    public static boolean isPackageInstalled(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

//    private static String cachedPrefix = null;

    public static String getWikiFolderNamePrefix(Context context) {
//        if (cachedPrefix == null) {
            String prefix = context.getFilesDir().toString() + "/";
//            cachedPrefix = prefix + "/" + context.getSharedPreferences("MenuActivity", Context.MODE_PRIVATE).getString("wiki_lang", "en") + "/";
            //noinspection UnnecessaryLocalVariable
            String cachedPrefix = prefix + "/" + context.getSharedPreferences("MenuActivity", Context.MODE_PRIVATE).getString("wiki_lang", "en") + "/";
            return cachedPrefix;
//        } else {
//            return cachedPrefix;
//        }
    }

    public static String getWikiFolderNamePrefix(Context context, String languageCode) {
        return context.getFilesDir().toString() + "/" + languageCode + "/";
    }

    public static void cleanCachedPrefix() {
//        cachedPrefix = null;
    }
}

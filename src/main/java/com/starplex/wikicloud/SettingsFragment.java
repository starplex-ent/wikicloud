package com.starplex.wikicloud;

import android.content.Context;
import android.content.SharedPreferences;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class SettingsFragment extends Fragment {

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AbstractWikiActivity activity = (AbstractWikiActivity) getActivity();
        getActivity().setTitle(activity.getString(AbstractWikiActivity.STRING_SETTINGS_TITLE));
        View layout = inflater.inflate(R.layout.fragment_settings, container, false);
        Button button = (Button) layout.findViewById(R.id.button);
        button.setText(activity.getString(AbstractWikiActivity.STRING_REINSTALL_WIKI_BUTTON));

        TextView versionTextView = (TextView) layout.findViewById(R.id.version_textview);
        SharedPreferences prefs = getActivity().getSharedPreferences("MenuActivity", Context.MODE_PRIVATE);
        int version = prefs.getInt("wiki_version", 1);
        String langCode = prefs.getString("wiki_lang", "");
        versionTextView.setText(String.format(activity.getString(AbstractWikiActivity.STRING_VERSION_FORMATTED), version + " " + langCode));

        return layout;
    }
}

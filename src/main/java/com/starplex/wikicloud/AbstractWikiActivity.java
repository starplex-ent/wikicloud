package com.starplex.wikicloud;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.MenuRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;

import java.util.Set;

public abstract class AbstractWikiActivity extends AppCompatActivity {
    public static final String STRING_SETTINGS_TITLE = "settings";
    public static final String STRING_VERSION_FORMATTED = "version_formatted_string";
    public static final String STRING_TOAST_ADDED_TO_FAVORITES = "added_to_favor";
    public static final String STRING_TOAST_REMOVED_FROM_FAVORITES = "removed_from_favor";
    public static final String STRING_SELECT_LANGUAGE_WIKI = "select_lang";
    public static final String STRING_NO_CONNECTION = "no_connection";
    public static final String STRING_RETRY_BUTTON = "retry";
    public static final String STRING_REINSTALL_WIKI_BUTTON = "reinstall_wiki";
    public static final String STRING_SEARCH_HINT = "search_hint";
    public static final String STRING_FAVORITES = "favorite";
    public static final String STRING_LIST_ITEM = "list_item";
    public static final String STRING_LOADING = "loading";
    public static final String STRING_CANNOT_DOWNLOAD_LANG = "cannot_download_language";
    public static final String STRING_OK = "ok";
    public static final String STRING_RATE_DIALOGUE_TITLE = "rate_dialogue_title";
    public static final String STRING_RATE_DIALOGUE_TEXT = "rate_dialogue_text";
    public static final String STRING_NEW_UPDATE = "new_update";
    public static final String STRING_UPDATE_DESCRIPTION = "update_desc";
    public static final String STRING_UPDATE_LATER = "update_later";
    public static final String STRING_UPDATE = "update";
    public static final String STRING_ALREADY_PURCHASED = "already_purchased";
    public static final String STRING_RESTART_APP = "restart_app";
    public static final String STRING_DOWNLOADING_WIKI_FAILED = "downloading_wiki_failed";
    public static final String STRING_REINSTALL_WIKI_QUESTION_TITLE = "reinstall_wiki_question_title";
    public static final String STRING_REINSTALL_WIKI_QUESTION_MESSAGE = "reinstall_wiki_question_message";
    public static final String STRING_REINSTALL = "reinstall";
    public static final String STRING_CANCEL = "cancel";
    public static final String STRING_DONT_ASK = "dont_ask";
    public static final String STRING_APP_TITLE = "app_title";
    public static final String STRING_SELECT_SECTION = "select_section";
    public static final String STRINGID_DRAWER_OPEN = "drawer_open";
    public static final String STRINGID_DRAWER_CLOSE = "drawer_close";

    public abstract String getString(String id);

    @StringRes
    public abstract int getStringId(String id);

    @MenuRes
    public abstract int getSearchMenuId();
    @MenuRes
    public abstract int getWebMenuId();
    @MenuRes
    public abstract int getWebAddedMenuId();
    @MenuRes
    public abstract int getWebBrowserMenuId();
    @IdRes
    public abstract int getZoomInButtonId();
    @IdRes
    public abstract int getZoomOutButtonId();
    @IdRes
    public abstract int getAddToFavoritesButtonId();
    @IdRes
    public abstract int getRemoveFromFavoritesButtonId();

    @DrawableRes
    public abstract int getIconId();

    public abstract Drawable getBackgroundForMenu();

    public abstract String getAppURLOnGooglePlay();

    public abstract String getWikiPackageName();

    public abstract Set<String> getWikiTranslationLanguagesList();

    public abstract String getStableUpdateSourceURL();

    public abstract String getAlphaUpdateSourceURL();

    public abstract String getAdApplicationId();

    public abstract String getAdUnitId();
}
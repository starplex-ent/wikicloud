package com.starplex.wikicloud;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadingRetainedFragment extends Fragment {

    public static final String TAG = "DOWNLOADING_TASK";

    public static final String ARG_CODE = "code";
    public static final String ARG_LINK = "link";
    public static final String ARG_PATH = "path";
    public static final String ARG_VERSION = "version";

    private OnDownloadingFragmentInteractionListener mListener;
    private WikiDownloadTask wikiDownloadTask;

    public DownloadingRetainedFragment() {
    }

    public static DownloadingRetainedFragment newInstance(String code, String link, String path, int version) {
        DownloadingRetainedFragment fragment = new DownloadingRetainedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CODE, code);
        args.putString(ARG_LINK, link);
        args.putString(ARG_PATH, path);
        args.putInt(ARG_VERSION, version);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        wikiDownloadTask = new WikiDownloadTask(this);
        wikiDownloadTask.execute(getArguments());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDownloadingFragmentInteractionListener) {
            mListener = (OnDownloadingFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void publishProgress(final int progress) {
        if (mListener != null && getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mListener.onDownloadingProgress(progress);
                }
            });
        }
    }

    public void publishResult(final Boolean result, final String langCode, final int version) {
        if (mListener != null && getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (result) {
                        mListener.onDownloadingCompleted(langCode, version);
                    } else {
                        mListener.onDownloadingFailed();
                    }
                }
            });
        }
    }

    public interface OnDownloadingFragmentInteractionListener {
        void onDownloadingProgress(int progress);
        void onDownloadingCompleted(String langCode, int version);
        void onDownloadingFailed();
    }
}

class WikiDownloadTask extends AsyncTask<Bundle, Integer, Void> {

    DownloadingRetainedFragment retainedFragment;

    public WikiDownloadTask(DownloadingRetainedFragment fragment) {
        super();

        retainedFragment = fragment;
    }

    @Override
    protected Void doInBackground(Bundle... params) {
        String code = params[0].getString(DownloadingRetainedFragment.ARG_CODE);
        String link = params[0].getString(DownloadingRetainedFragment.ARG_LINK);
        String path = params[0].getString(DownloadingRetainedFragment.ARG_PATH);

        int count;

        try {
            URL url = new URL(link);
            URLConnection connection = url.openConnection();
            connection.connect();

            int lengthOfFile = connection.getContentLength();

            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            OutputStream outputStream = new FileOutputStream(path + "/" + code + ".zip");

            byte[] data = new byte[1024*10];

            long total = 0;

            while ((count = inputStream.read(data)) != -1) {
                total += count;
                retainedFragment.publishProgress((int)(total*100)/lengthOfFile);
                outputStream.write(data, 0, count);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();

            Decompress decompress = new Decompress(path + "/" + code + ".zip", path + "/" + code + "/");
            boolean decompress_result = decompress.unzip();
            File f = new File(path + "/" + code + ".zip");
            //noinspection ResultOfMethodCallIgnored
            f.delete();

            retainedFragment.publishProgress(105);

            try {
                Index index = SearchIndexer.index(retainedFragment.getContext(), path + "/" + code + "/main/index.xml", path + "/" + code, code);
                ObjectOutputStream indexOutputStream = new ObjectOutputStream(new FileOutputStream(path + "/" + code + "/search.index"));
                indexOutputStream.writeObject(index);
                indexOutputStream.close();
            } catch (IOException ignored) {
            }

            retainedFragment.publishResult(decompress_result, code, params[0].getInt("version"));
        } catch (java.io.IOException e) {
            retainedFragment.publishResult(false, null, 0);
        }

        return null;
    }
}
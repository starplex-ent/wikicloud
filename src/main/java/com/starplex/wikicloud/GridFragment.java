package com.starplex.wikicloud;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

public class GridFragment extends Fragment {

    private boolean enableBackground;

    @SuppressWarnings("FieldCanBeLocal")
    private GridAdapter gridAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceBundle) {
        View view = inflater.inflate(R.layout.grid_fragment, container, false);

        GridView gridView = (GridView) view.findViewById(R.id.gridView1);
        gridAdapter = new GridAdapter(getActivity(), getArguments().getString("url"));
        enableBackground = gridAdapter.enableBackground;
        if (gridAdapter.title != null) {
            getActivity().setTitle(gridAdapter.title);
        }
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new GridItemListener((MenuActivity) getActivity(), gridAdapter, false));

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (enableBackground) {
            AbstractWikiActivity activity = (AbstractWikiActivity) getActivity();
            inflater.inflate(activity.getSearchMenuId(), menu);
            SearchManager searchManager =
                    (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.search_bar).getActionView();
            searchView.setQueryHint(activity.getString(AbstractWikiActivity.STRING_SEARCH_HINT));
            //noinspection deprecation
            searchView.setSuggestionsAdapter(new SimpleCursorAdapter(getActivity(),
                    android.R.layout.simple_list_item_1, null,
                    new String[]{BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_INTENT_DATA},
                    new int[]{android.R.layout.simple_list_item_1}));
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getActivity().getComponentName()));

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        RefWatcher refWatcher = ((WikiApplication) getActivity().getApplication()).getRefWatcher();
//        (refWatcher).watch(gridAdapter);
//        for (int i = 0; i < gridAdapter.imgBMPs.length; i++) {
//            if (gridAdapter.imgBMPs[i] != null) {
//                refWatcher.watch(gridAdapter.imgBMPs[i]);
//            }
//        }
    }

}

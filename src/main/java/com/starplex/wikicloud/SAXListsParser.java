package com.starplex.wikicloud;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.starplex.wikicloud.CommonAdapter;
import com.starplex.wikicloud.Utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXListsParser extends DefaultHandler {

    final String main_tag = "table";
    final String item_tag = "item";
    final String img_attr = "img";
    final String type_attr = "type";
    final String text_attr = "text";

    CommonAdapter adapter;
    int current = 0;

    private String curstomLanguageCode = null;
    private boolean uiMode = true;

    private String cachedPath = null;

    public SAXListsParser(CommonAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void endDocument() throws SAXException {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void startElement(String namespaceURI,
                             String localName,
                             String qName,
                             Attributes atts) throws SAXException {
        if (localName.equals(main_tag)) {
            int count = Integer.parseInt(atts.getValue("count"));

            adapter.imgBMPs = new Bitmap[count];
            adapter.imgBMPPaths = new String[count];
            adapter.imgTypes = new CommonAdapter.ObjectType[count];
            adapter.imgStrs = new String[count];
            adapter.imgTexts = new String[count];

            adapter.title = atts.getValue("text");

            String enableBackground = atts.getValue("enableBackground");
            if (enableBackground != null) {
                if (enableBackground.equals("true"))
                    adapter.enableBackground = true;
            } else {
                adapter.enableBackground = false;
            }
        } else if (localName.equals(item_tag)) {
            String fs = atts.getValue(img_attr);
            String type = atts.getValue(type_attr);

            adapter.imgTexts[current] = atts.getValue(text_attr);
            if (adapter.context != null) {
                if (cachedPath == null) {
                    cachedPath = (curstomLanguageCode==null
                            ? Utils.getWikiFolderNamePrefix(adapter.context)
                            :Utils.getWikiFolderNamePrefix(adapter.context, curstomLanguageCode));
                }
                String path = cachedPath + fs;
                adapter.imgBMPPaths[current] = path;
                if (uiMode) {
                    adapter.imgBMPs[current] = BitmapFactory.decodeFile(path);
                }
            }

            if (type == null)
                adapter.imgTypes[current] = CommonAdapter.ObjectType.NONE;
            else {
                switch (type) {
                    case "web":
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.WEB_LINK;

                        adapter.imgStrs[current] = atts.getValue("link");
                        break;
                    case "grid_link":
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.GRID_LINK;

                        adapter.imgStrs[current] = atts.getValue("link");
                        break;
                    case "ads":
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.ADS;
                        break;
                    case "favor":
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.FAVOR;
                        break;
                    case "article":
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.ARTICLE;
                        adapter.imgStrs[current] = atts.getValue("link");
                        break;
                    case "external_app":
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.EXTERNAL_APP;
                        adapter.imgStrs[current] = atts.getValue("link");
                        break;
                    case "settings":
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.SETTINGS;
                        break;
                    case "none":
                    default:
                        adapter.imgTypes[current] = CommonAdapter.ObjectType.NONE;
                        break;
                }
            }

            current++;
        }
    }

    public void setUiModeEnabled(boolean uiMode) {
        this.uiMode = uiMode;
    }

    public void setCurstomLanguageCode(String curstomLanguageCode) {
        this.curstomLanguageCode = curstomLanguageCode;
    }
}


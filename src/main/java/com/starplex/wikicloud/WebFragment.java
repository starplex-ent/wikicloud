package com.starplex.wikicloud;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class WebFragment extends Fragment {
	String url;
    WebView webView;
	
	@SuppressLint("SetJavaScriptEnabled")
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							Bundle savedInstanceBundle) {
		View view = inflater.inflate(R.layout.web_fragment, container, false);
		
		webView = (WebView)view.findViewById(R.id.webView1);
		url = getArguments().getString("url");
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
		webView.loadUrl(url);
		webView.setWebViewClient(new WebViewClient() {
			@SuppressWarnings("deprecation")
			@Override
			public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
				if (url.startsWith("file:///android_asset/") && (url.endsWith("png") || url.endsWith("jpg") || url.endsWith("css") || url.endsWith("js"))) {
					try {
						//noinspection ConstantConditions
						return new WebResourceResponse(MimeTypeMap.getFileExtensionFromUrl(url), "UTF-8", new FileInputStream(url.replace("file:///android_asset/", Utils.getWikiFolderNamePrefix(getActivity()))));
					} catch (FileNotFoundException e) {
						return null;
					}
				}
				return null;
			}

			@Override
			public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
				if (Build.VERSION.SDK_INT >= 21) {
					return shouldInterceptRequest(view, request.getUrl().toString());
				}
				return null;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				try {
					WebFragment.this.getActivity().setTitle(view.getTitle());
				} catch (NullPointerException e) {
					// if you add there Log.e or others app will crash
					// don't do that!!!
				}
			}
		});
		setHasOptionsMenu(true);
		
		return view;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuActivity activity = (MenuActivity) getActivity();

        inflater.inflate(activity.getWebBrowserMenuId(), menu);

		synchronized (activity.dbMutex) {
			for (int i = 0; i < activity.favouriteArticles.size(); i++) {
				if (activity.favouriteArticles.get(i).article_path.equals(url.replace("file://" + Utils.getWikiFolderNamePrefix(getActivity()), ""))) {
					inflater.inflate(activity.getWebAddedMenuId(), menu);
					return;
				}
			}
		}
		
		inflater.inflate(activity.getWebMenuId(), menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		MenuActivity activity = (MenuActivity)getActivity();

        String urlWithoutPrefix = url.replace("file://"+Utils.getWikiFolderNamePrefix(getActivity()), "");

		int i1 = item.getItemId();
		if (i1 == activity.getZoomInButtonId()) {
			webView.zoomIn();

		} else if (i1 == activity.getZoomOutButtonId()) {
			webView.zoomOut();

		} else if (i1 == activity.getAddToFavoritesButtonId()) {
			synchronized (activity.dbMutex) {
				SQLiteDatabase db = activity.dbHelper.getWritableDatabase();
				ContentValues cv = new ContentValues();
				cv.put(FavouriteArticlesDatabase.ARTICLE_TITLE, activity.getTitle().toString());
				cv.put(FavouriteArticlesDatabase.ARTICLE_URL, urlWithoutPrefix);
				long id = db.insert(FavouriteArticlesDatabase.TABLE_NAME, FavouriteArticlesDatabase.ARTICLE_TITLE, cv);
				db.close();

				FavouriteArticle favor = new FavouriteArticle();
				favor.title = activity.getTitle().toString();
				//noinspection ConstantConditions
				favor.article_path = url.replace("file://" + Utils.getWikiFolderNamePrefix(getActivity()), "");
				favor.id = id;
				activity.favouriteArticles.add(favor);
			}
			activity.invalidateOptionsMenu();
			Toast.makeText(activity, activity.getString(AbstractWikiActivity.STRING_TOAST_ADDED_TO_FAVORITES), Toast.LENGTH_SHORT).show();

		} else if (i1 == activity.getRemoveFromFavoritesButtonId()) {
			synchronized (activity.dbMutex) {
				SQLiteDatabase db = activity.dbHelper.getWritableDatabase();

				FavouriteArticle article = null;
				for (int i = 0; i < activity.favouriteArticles.size(); i++) {
					if (activity.favouriteArticles.get(i).article_path.equals(urlWithoutPrefix)) {
						article = activity.favouriteArticles.get(i);
					}
				}
				if (article != null) {
					String selection = FavouriteArticlesDatabase.UID + " LIKE ?";
					String[] selectionArgs = {String.valueOf(article.id)};
					// Issue SQL statement.
					db.delete(FavouriteArticlesDatabase.TABLE_NAME, selection, selectionArgs);
					db.close();

					activity.favouriteArticles.remove(article);
				}
			}

			activity.invalidateOptionsMenu();

			Toast.makeText(activity, activity.getString(AbstractWikiActivity.STRING_TOAST_REMOVED_FROM_FAVORITES), Toast.LENGTH_SHORT).show();

		}
		
		return super.onOptionsItemSelected(item);
	}
}

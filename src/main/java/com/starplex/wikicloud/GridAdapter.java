package com.starplex.wikicloud;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class GridAdapter extends CommonAdapter {
    public GridAdapter(Context c, String indexFileName) {
        super(c, indexFileName, false, null);
    }

    public GridAdapter(Context c, String indexFileName, boolean loadImages, String customLanguageCode) {
        super(c, indexFileName, loadImages, customLanguageCode);
    }

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        if (enableBackground && parent.getBackground() == null) {
            Drawable backgroundForMenu = ((AbstractWikiActivity) parent.getContext()).getBackgroundForMenu();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                parent.setBackground(backgroundForMenu);
            } else {
                parent.setBackgroundDrawable(backgroundForMenu);
            }
        }

        if (imgBMPs[position] == null) {
            imgBMPs[position] = BitmapFactory.decodeFile(imgBMPPaths[position]);
        }

        if (imgTexts[position] != null) {
            View view;
            if (convertView == null) {
                view = View.inflate(context, R.layout.grid_item, null);
            } else {
                view = convertView;
            }
            TextView textView = (TextView) view.findViewById(R.id.grid_item_label);
            textView.setText(imgTexts[position]);
            ImageView imageView = (ImageView) view.findViewById(R.id.grid_item_image);
            //imageView.setImageDrawable(new BitmapDrawable(imgBMPs[position]));
            imageView.setImageBitmap(imgBMPs[position]);
            return view;
        } else {
            ImageView imageView;
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                imageView = new ImageView(context);
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                imageView.setPadding(3, 3, 3, 3);
                imageView.setAdjustViewBounds(true);
                if (Build.VERSION.SDK_INT >= 16)
                    imageView.setBackground(context.getResources().getDrawable(R.drawable.grid_item_background));
                else
                    imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.grid_item_background));
            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setImageBitmap(imgBMPs[position]);

            return imageView;
        }
	}
}

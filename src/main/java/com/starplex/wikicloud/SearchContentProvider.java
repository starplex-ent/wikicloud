package com.starplex.wikicloud;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class SearchContentProvider extends ContentProvider {
    private Index searchIndex;
    private String indexPath;

    public SearchContentProvider() {
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getType(@NonNull Uri uri) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public boolean onCreate() {
        try {
            indexPath = Utils.getWikiFolderNamePrefix(getContext()) + "search.index";
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(indexPath));
            searchIndex = (Index) ois.readObject();
            ois.close();
        } catch (ClassNotFoundException | IOException e) {
            return false;
        }

        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        if (searchIndex == null || (indexPath != null && !indexPath.equals(Utils.getWikiFolderNamePrefix(getContext()) + "search.index"))) {
            onCreate();
        }

        //noinspection ConstantConditions
        String prefix = "content://" + getContext().getPackageName() + ".SearchContentProvider" + "/" + SearchManager.SUGGEST_URI_PATH_QUERY;
        if (uri.toString().startsWith(prefix))
        {
            String request = uri.getLastPathSegment().toLowerCase().trim();

            String[] columns = new String[] {BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_ICON_1, SearchManager.SUGGEST_COLUMN_INTENT_DATA};
            MatrixCursor cursor = new MatrixCursor(columns);

            int id = 0;
            for (int i = 0; i < searchIndex.articlePaths.size(); i++) {
                if (searchIndex.articleTitles.get(i).toLowerCase().contains(request)) {
                    cursor.addRow(new Object[]{(Integer.toString(id)), searchIndex.articleTitles.get(i), "file://" + searchIndex.articleImagePaths.get(i), searchIndex.articlePaths.get(i)});
                    id++;
                }
            }

            return cursor;
        } else {
            throw new UnsupportedOperationException("Not supported");
        }
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported");
    }
}

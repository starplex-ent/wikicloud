package com.starplex.wikicloud;

import android.content.Context;
import android.support.annotation.NonNull;

import com.starplex.wikicloud.CommonAdapter;
import com.starplex.wikicloud.GridAdapter;
import com.starplex.wikicloud.Index;

import java.util.Iterator;

/**
 * Class creating index for the downloaded wiki to make search on it available
 */
public class SearchIndexer {

    private static void index(Context c, Index index, String root, String basePath, String code) {
        GridAdapter gridAdapter = new GridAdapter(c, root, false, code);

        for (int i = 0; i < gridAdapter.imgTypes.length; i++) {
            if (gridAdapter.imgTypes[i] == CommonAdapter.ObjectType.ARTICLE && gridAdapter.imgTexts[i] != null) {
                index.articleTitles.add(gridAdapter.imgTexts[i]);
                index.articlePaths.add(gridAdapter.imgStrs[i]);
                index.articleImagePaths.add(gridAdapter.imgBMPPaths[i]);
            }
        }
        for (int i = 0; i < gridAdapter.imgTypes.length; i++) {
            if (gridAdapter.imgTypes[i] == CommonAdapter.ObjectType.GRID_LINK) {
                index(c, index, basePath+"/"+gridAdapter.imgStrs[i], basePath, code);
            }
        }
    }

    /**
     * Indexes wiki section recursively
     * @param root absolute path to index.xml that needs to be indexed
     * @param basePath absolute path to the root wiki folder
     * @return index of articles in section
     */
    public static Index index(Context c, String root, String basePath, String code) {
        Index index = new Index();

        index(c, index, root, basePath, code);

        return index;
    }

    /**
     * Searches articles with title containing given string
     * @param index index where we search
     * @param searchString string we are looking for in the index
     * @return {@link Index} containing articles found
     */
    @NonNull
    public static Index search(Index index, @NonNull String searchString) {
        Index foundArticles = new Index();

        for (Iterator<String> i = index.articlePaths.iterator(), p = index.articleTitles.iterator(),
                img = index.articleImagePaths.iterator(); i.hasNext(); ) {
            String title = p.next().toLowerCase();
            String path = i.next();
            String imgPath = img.next();

            if (title.contains(searchString.toLowerCase())) {
                foundArticles.articlePaths.add(path);
                foundArticles.articleTitles.add(title);
                foundArticles.articleImagePaths.add(imgPath);
            }
        }

        return foundArticles;
    }
}

